Names:
Levi Rak
Kyle Busch
Conner Becker
Kyle Melton

Specific Contributions:
20%	Levi Rak: Built test cases
5%	Kyle Busch: Started UML Diagram
37.5% Conner Becker: Created GUI elements, helped with game code
37.5% Kyle Melton: Created game code, helped with GUI elements, finished UML 

Our code is dependent on junit4.jar. You may be required to include it on the classpath or if you load the project in to an IDE use the IDE's automatic classpath builder to include the junit4.jar.

Another thing to note. If you run from an IDE the IDE may screw up how filepaths
are handled and the tokens may default to black squares. If you compile from the 
command line the tokens should should up as their actual images due to the 
filepath being relative to the java package and not the IDE project directory.

To run our code from the command line ensure you are in 
the team4/pa2-4/ directory and do the following (if unable to compile please
contact conner becker):

cd src/
javac esof322/monopoly/Driver.java
java esof322/monopoly/Driver

Strengths:

The GUI is a lot less reliant on JOptionPanes and now has functional controls.
99% of monopoly has been implemented now. WE CAN SELL PROPERTIES!!!
Strong use of pair programming (Kyle M, Conner B) to ensure GUI and game code work together almost seamlessly.
Factories can generate new themes for the game withing minutes.


Weaknesses:

Cant mortgage property.
Determining winner at the end only considers who has the most money and does not
factor in the properties owned by everyone.
What was rolled on the dice is not displayed.



